<?php

/**
 * Base class
 *
 * @author Juan Miguel sosso <jmsosso@binfactory.com>
 */
abstract class DMCBase
{
  protected $items = array();

  protected abstract function construct();

  /**
   * Returns the array. construct is called before to add items to the array.
   * @return array Array with the items for menu, page or form.
   */
  public function get($args = array())
  {
    call_user_func_array(array($this, 'construct'), $args);
    //$this->construct();
    return $this->items;
  }


  /**
   * Sets the page title.
   * @param string $title Page title.
   */
  protected function setTitle($title) {
    drupal_set_title($title);
  }
}

