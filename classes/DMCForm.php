<?php

/**
 * Creates a DMC 7 form
 *
 * @author Juan Miguel sosso <jmsosso@binfactory.com>
 */
abstract class DMCForm extends DMCBase
{
  private $state;
  protected $values = NULL;
  protected $triggeringElement = NULL;

  public function __construct($form, &$state)
  {
    $this->items  = $form;
    $this->state = $state;

    $this->items['#attributes'] = array('id' => array(get_class($this)));

    if (isset($state['values']))
      $this->values = $state['values'];

    if (isset($state['triggering_element']))
      $this->triggeringElement = $state['triggering_element'];
  }


  protected function setClass(array $classes) {
    $this->items['#attributes']['class'] = $classes;
  }


  /**
   * Adds a new item to the form.
   * @param DMCFormItem $item Item to add, must be a descendant of DMCFormItem.
   * @return DMCFormItem The new added item.
   */
  public function add(DMCFormItem $item)
  {
    $this->items = array_merge($this->items, $item->getItem());
    return $item;
  }


  function values($key)
  {
    if (isset($this->state['values'][$key]))
      return $this->state['values'][$key];
    else
      return NULL;
  }

  function completeForm()
  {
    if (isset($this->state['complete form']))
      return $this->state['complete form'];
    else
      return NULL;
  }
}

