<?php

/**
 * Creates a DMC 7 form
 *
 * @author Juan Miguel sosso <jmsosso@binfactory.com>
 */

/**
 * Generic form item
 */
class DMCFormItem
{
  protected $item = array();
  protected $id;

  public function __construct($id) {
    $this->id = $id;
  }


  public function getItem() {
    return $this->item;
  }

  public function getHTML()
  {
    $item = array_merge($this->item[$this->id], array(
        '#id' => $this->id));
    return drupal_render($item);
  }


  public function setDescription($description)
  {
    $this->item[$this->id]['#description'] = t($description);
    return $this;
  }


  public function setEnable($enabled)
  {
    $this->item[$this->id]['#disabled'] = !$enabled;
    return $this;
  }


  public function setWrapper($prefix, $sufix = '')
  {
    $this->item[$this->id]['#prefix'] = $prefix;
    $this->item[$this->id]['#suffix'] = $sufix;

    return $this;
  }


  public function setAjax($callback, $wrapper)
  {
    $this->item[$this->id]['#ajax'] = array(
        'callback' => $callback,
        'wrapper' => $wrapper,
        'effect' => 'fade');
    return $this;
  }
}


/**
 * TextBox form item
 */
class DMCFormTextBox extends DMCFormItem
{
  /**
   * Creates a new textBox (textfield)
   * @param string $id Field's Id
   * @param string $title Field's title
   * @param string $default Default value for the field (Optional)
   */
  public function __construct($id, $title, $default = '')
  {
    parent::__construct($id);

    $this->item[$id] = array(
      '#type' => 'textfield',
      '#title' => t($title),
      '#default_value' => $default,
    );
  }
}


/**
 * Select form item
 */
class DMCFormSelect extends DMCFormItem
{
  /**
   * Creates a new select
   * @param string $id Field's Id
   * @param string $title Field's title
   * @param string $options Selectable options for a form element that allows multiple choices.
   * @param integer $default Default value for the field (Optional)
   */
  public function __construct($id, $title, $options, $default = 0)
  {
    parent::__construct($id);

    $this->item[$id] = array(
      '#type' => 'select',
      '#title' => t($title),
      '#options' => $options,
      '#default_value' => $default,
    );
  }
}


/**
 * Table select form item
 */
class DMCFormTableSelect extends DMCFormItem
{
  public function __construct($id, $header, $options)
  {
    parent::__construct($id);

    $this->item[$id] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
    );
  }


  /**
   * Text to display if the $options parameter is empty
   * @param string $text Text to display
   */
  public function setEmptyText($text)
  {
    $this->item[$this->id]['#empty'] = t($text);
    return $this;
  }
}


/**
 * Radio form item
 */
class DMCFormRadio extends DMCFormItem
{
  /**
   * Creates a radio button.
   * @param string $id Field's Id
   * @param string $title Field's title
   * @param string $name Name attribute of the radio button.
   */
  public function __construct($id, $title, $name = '')
  {
    parent::__construct($id);

    $this->item[$id] = array(
      '#type' => 'radio',
      '#title' => t($title),
    );

    if ($name)
      $this->item[$id]['#name'] = $name;
  }
}


/**
 * Radios form item
 */
class DMCFormRadios extends DMCFormItem
{
  /**
   * Creates a set of radio buttons.
   * @param string $id Field's Id
   * @param string $title Field's title
   * @param string $options Selectable options for a form element that allows multiple choices.
   * @param integer $default Default value for the field (Optional)
   */
  public function __construct($id, $title, $options, $default = 0)
  {
    parent::__construct($id);

    $this->item[$id] = array(
      '#type' => 'radios',
      '#title' => t($title),
      '#options' => $options,
      '#default_value' => $default,
    );
  }
}


/**
 * Button form item
 */
class DMCFormButton extends DMCFormItem
{
  public function __construct($id, $caption, $submit = true)
  {
    parent::__construct($id);

    $this->item[$id] = array(
      '#type' => 'button',
      '#value' => t($caption),
      '#executes_submit_callback' => $submit,
    );

    if (!$submit)
      $this->item[$id]['#attributes'] = array('onclick' => 'return (false);');
  }
}


/**
 * Markup form item
 */
class DMCFormMarkup extends DMCFormItem
{
  /**
   * Creates a new markup
   * @param string $id Field's Id
   * @param string $markup Used to set HTML that will be output on the form.
   */
  public function __construct($id, $markup)
  {
    parent::__construct($id);

    $this->item[$id] = array(
      '#type' => 'markup',
      '#markup' => $markup,
    );
  }
}
