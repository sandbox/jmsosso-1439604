<?php
/**
 * Creates a Drupal menu item.
 *
 * @author Juan Miguel sosso <jmsosso@binfactory.com>
 */
class DMCMenuItem
{
  private $path;
  private $class;
  private $item;

  /**
   * Creates a new menu item.
   * @param string $path Menu path.
   * @param string $class Class to instantiate when the user goes to that path.
   * @param boolean $anonymousAccess Allow access to anonymous users or only to authenticates ones.
   */
  public function __construct($path, $class, $anonymousAccess = TRUE)
  {
    $this->path  = $path;
    $this->class = $class;

    $this->item = array(
        'access callback' => $anonymousAccess? TRUE: 'user_is_logged_in',
    );
  }

  public function setTitle($title) {
    $this->item['title'] = t($title);
  }

  /**
   * Include the path in a Drupal menu.
   * @param string $menuName Drupal menu name.
   */
  public function setMenu($menuName, $description = NULL)
  {
    $this->item['type']      = MENU_NORMAL_ITEM;
    $this->item['menu_name'] = $menuName;

    if ($description)
      $this->item['description'] = t($description);
  }


  public function addTo(array &$items)
  {
    if (is_subclass_of($this->class, 'DMCPage')) {
      $this->item['page callback']  = 'dmc_get_page';
      $this->item['page arguments'] = array($this->class);
    }
    elseif (is_subclass_of($this->class, 'DMCForm')) {
      $this->item['page callback']  = 'drupal_get_form';
      $this->item['page arguments'] = array('dmc_get_form', $this->class);
    }

    //debug($item);
    $items[$this->path] = $this->item;
  }
}


interface DMCMenu
{
  static public function getMenuItem();
  // TODO: Quizá debo poner una funcion de control de acceso aquí, por si se quieren permisos dinámicos
}