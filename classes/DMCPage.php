<?php

/**
 * Clase base para heredar y crear páginas para módulos Drupal.
 * @author Juan Miguel Sosso <jmsosso@binfactory.com>
 */
abstract class DMCPage extends DMCBase
{
  protected function readStatic($name)
  {
    if (isset($_SESSION[$name]))
      $this->{$name} = $_SESSION[$name];
  }

  protected function writeStatic($name)
  {
    $_SESSION[$name] = $this->{$name};
  }

  public static function text2web($text)
  {
    return nl2br(htmlspecialchars($text));
  }

  /**
   * Returs a HTML string containig $content text inside $tag tags.
   * @param string $tag HTML tag
   * @param string $content Text to insert inside HTML tag
   * @param string $options Optional tag options
   * @return string <tag>content</tag>
   */
  public static function htmlTag($tag, $content, $options = FALSE)
  {
    if ($options)
      return "<$tag $options>$content</$tag>";
    else
      return "<$tag>$content</$tag>";
  }
}
