<?php

/**
 * Creates a Drupal 7 HTML table.
 * @author Juan Miguel Sosso <jmsosso@binfactory.com>
 */
class DMCTable
{
  public $caption   = '';
  public $sticky    = TRUE;
  public $emptyText = 'Tabla vacía';

  private $attributes = array();
  private $header     = array();
  private $rows       = array();
  private $rowClass   = array();


  /**
   * Construye un objeto para crear una tabla HTML.
   * @param string $id ID de la tabla
   * @param string $class Clase de la tabla
   */
  function __construct($id = FALSE, $class = '')
  {
    if ($id)
      $this->setId($id);

    if ($class)
      $this->setClass($class);
  }


  public function setId($id) {
    $this->attributes['id'] = $id;
  }

  public function setClass($class) {
    $this->attributes['class'][] = $class;
  }

  public function setHeader($header) {
    if (is_array($header))
      $this->header = $header;
    else
      $this->header = func_get_args();
  }

  public function addRow($row, $rowClass = FALSE) {
    $this->rows[] = $row;
    if ($rowClass)
      $this->setRowClass (count($this->rows) - 1, $rowClass);
  }

  public function getRows() {
    return $this->rows;
  }

  public function setRow($key, $value) {
    $this->rows[$key] = $value;
  }

  public function clear() {
    $this->rows = array();
  }

  public function setRowClass($index, $class) {
    $this->rowClass[$index] = $class;
  }

  public function getHTML() {
    $structure = array();

    foreach ($this->rows as $index => $row) {
      $newRow = array();
      foreach ($row as $cell) {
        $newRow[] = array('data' => $cell);
      }
      if (isset($this->rowClass[$index]))
        $structure[] = array('data' => $newRow, 'class' => array($this->rowClass[$index]));
      else
        $structure[] = $newRow;
    }

    return theme('table', array(
        'header' => $this->header,
        'rows' => $structure,
        'attributes' => $this->attributes,
        'sticky' => $this->sticky,
        'caption' => $this->caption,
        'colgroups' => array(),
        'empty' => $this->emptyText)
    );
  }
}
